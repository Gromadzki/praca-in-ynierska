﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Pogodynka
{
    [Serializable]
    public class Pogodynka : IPogodynka.IPogodynka
    {
        [NonSerialized]
        WebClient client = new WebClient();
        string htmlPogoda = string.Empty;
        string adresPogoda = "http://www.meteoprog.pl/pl/meteograms/Warszawa/";
        
        public Pogodynka()
        {
        }

        public void Aktualizuj(int Godzina)
        {
            if (htmlPogoda==string.Empty)
            {
                Pobierz();
            }

            AktualizujTemperaturę(Godzina);
            AktualizujOpady(Godzina);
        }

        public void Pobierz()
        {
            try
            {
                htmlPogoda = client.DownloadString(adresPogoda);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        void AktualizujTemperaturę(int Godzina)
        {
        }

        void AktualizujOpady(int Godzina)
        {

        }



        public int PodajTemperaturę(int Godzina)
        {

            if (htmlPogoda == string.Empty)
            {
                Pobierz();
            }

            int start = htmlPogoda.IndexOf("meteoForecast");
            int koniec = htmlPogoda.IndexOf("<span class=\"dayName\">Jutro</span>");
            string s123 = htmlPogoda.Substring(start, koniec - start);
            s123 = s123.Substring(s123.IndexOf("<tr align=\"center\" class=\"colorRow\">")); //redukcja wierszy
            string[] tablica;
            List<string> lista = new List<string>();
            tablica = s123.Split("<td>".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            List<int> inty = new List<int>();
            foreach (var item in tablica)
            {
                if (item.Contains("00") || item.Contains('°'))
                {
                    lista.Add(item);
                }
            }
            for (int i = lista.Count - 1; i > 0; i--)
            {
                if ((i + 1) % 3 == 0)
                {
                    lista.RemoveAt(i);
                }

            }
            for (int i = 0; i < lista.Count; i++)
            {
                if (i % 2 != 0)
                {

                    inty.Add(Convert.ToInt32(lista[i].Substring(0, 2)));
                }
            }
            return inty[Godzina];
        }

        public double PodajOpady(int Godzina)
        {

            throw new NotImplementedException();
        }

        public bool CzyDzień(int Godzina)
        {
            throw new NotImplementedException();
        }


        public void PobierzOpady()
        {
            throw new NotImplementedException();
        }

        public void PobierzGodziny()
        {
            throw new NotImplementedException();
        }
    }
}
[Serializable]
public class Pogoda 
{
    private List<int> obliczListe()
    {
        WebClient client = new WebClient();
        string adres2 = "http://www.meteoprog.pl/pl/meteograms/Warszawa/";
        string html = string.Empty;
        List<int> inty = new List<int>(48);
        try
        {
            html = client.DownloadString(adres2);

            int start = html.IndexOf("meteoforecast");
            int koniec = html.IndexOf("<span class=\"dayName\">Jutro</span>");
            string s123 = html.Substring(start, koniec - start);
            s123 = s123.Substring(s123.IndexOf("<tr align=\"center\" class=\"colorRow\">")); //redukcja wierszy
            string[] tablica;
            List<string> lista = new List<string>();
            tablica = s123.Split("<td>".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            foreach (var item in tablica)
            {
                if (item.Contains("00") || item.Contains('°'))
                {
                    lista.Add(item);
                }
            }
            for (int i = lista.Count - 1; i > 0; i--)
            {
                if ((i + 1) % 3 == 0)
                {
                    lista.RemoveAt(i);
                }

            }
            for (int i = 0; i < lista.Count; i++)
            {
                if (i % 2 == 0)
                {
                    string s = lista[i].Remove(2);
                    inty.Add(Convert.ToInt32(s));
                }
                else
                {
                    inty.Add(Convert.ToInt32(lista[i].Substring(1, 2)));
                }
            }
        }

        catch (Exception)
        {
            Console.WriteLine("nie mogę pobrać danych");
        }

        return inty;
    }
    public List<int> ListaPogody()
    {
        List<int> lista = obliczListe();
        for (int i = lista.Count - 1; i > 0; i--)
        {
            if (i % 2 == 0)
            {
                lista.RemoveAt(i);

            }
        }
        return lista;
    }

    public List<double> ListaOpadów()
    {
        WebClient client = new WebClient();
        string adres2 = "http://www.meteoprog.pl/pl/meteograms/Warszawa/";
        string html = string.Empty;
        List<double> inty = new List<double>(48);
        html = client.DownloadString(adres2);

        int start = html.IndexOf("meteoforecast");
        int koniec = html.IndexOf("<span class=\"dayName\">Jutro</span>");
        string s123 = html.Substring(start, koniec - start);
        s123 = s123.Substring(s123.IndexOf("<tr align=\"center\" class=\"colorRow\">")); //redukcja wierszy
        string[] tablica;
        List<string> lista = new List<string>();
        tablica = s123.Split("<td>".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

        foreach (var item in tablica)
        {
            if (item.Contains("00") || item.Contains("mm"))
            {
                lista.Add(item);
            }
        }
        for (int i = lista.Count - 1; i > 0; i--)
        {
            if ((i + 1) % 3 == 0)
            {
                lista.RemoveAt(i);
            }

        }
        for (int i = 0; i < lista.Count; i++)
        {
            if (i % 2 == 0)
            {
                string s = lista[i].Remove(2);
                inty.Add(Convert.ToInt32(s));
            }
            else
            {
                inty.Add(Convert.ToDouble(lista[i].Remove(lista[i].Length - 3)));
            }
        }
        foreach (var item in inty)
        {
            Console.WriteLine(item);
        }

        return inty;
    }
  
}