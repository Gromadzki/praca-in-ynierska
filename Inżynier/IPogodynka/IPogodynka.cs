﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPogodynka
{
    
    public interface IPogodynka
    {

        void Aktualizuj(int Godzina);
        int PodajTemperaturę(int Godzina);
        double PodajOpady(int Godzina);
        bool CzyDzień(int Godzina);
        void PobierzOpady();
        void PobierzGodziny();
    }
}
