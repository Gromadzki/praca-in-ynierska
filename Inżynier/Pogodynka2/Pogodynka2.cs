﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ForecastIOPortable;
using System.Net;
using System.Linq;

namespace Pogodynka2
{
    [Serializable]
    public class Pogodynka2 : IPogodynka.IPogodynka
    {
        [NonSerialized]
        ForecastApi api;
        List<double> opady;
        DateTime wschód;
        DateTime zachód;

        [NonSerialized]
        WebClient client = new WebClient();
        
        public Pogodynka2()
        {
            api = new ForecastApi("ebba06dcb6f40c995ca5b126f214b9c2");
            opady = new List<double>();
        }
    
        public void Aktualizuj(int Godzina)
        {
             var   prognoza =  api.GetWeatherDataAsync(52.2287, 21.0063);
        }

        public int PodajTemperaturę(int Godzina)
        {
            ForecastIOPortable.ForecastApi api = new ForecastIOPortable.ForecastApi("ebba06dcb6f40c995ca5b126f214b9c2");

            var forecast = api.GetWeatherDataAsync(52.2287, 21.0063);
            var results = forecast.Result;
            int tempC = (int)(5.0 / 9.0 * (results.Currently.Temperature - 32));
            return tempC;
            
           
        }

        public double PodajOpady(int Godzina)
        {
            if (opady.Count!=0)
            {
                 return opady[Godzina];

            }
            PobierzOpady();
            return opady[Godzina];
        }

        public bool CzyDzień(int Godzina)
        {
            if (wschód.Year<2000)
            {
                PobierzGodziny();
            }
            if (DateTime.Now.CompareTo(wschód) < 0 || DateTime.Now.CompareTo(zachód) > 0)
            {
                return false;
            }
            return true;
        }

        public void PobierzOpady()
        {
                string adres2 = "http://www.meteoprog.pl/pl/meteograms/Warszawa/";
                string html = string.Empty;
                html = client.DownloadString(adres2);

                int start = html.IndexOf("meteoforecast");
                int koniec = html.IndexOf("<span class=\"dayName\">Jutro</span>");
                string s123 = html.Substring(start, koniec - start);
                s123 = s123.Substring(s123.IndexOf("<tr align=\"center\" class=\"colorRow\">")); //redukcja wierszy
                string[] tablica;
                List<string> lista = new List<string>();
                tablica = s123.Split("<td>".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                lista = tablica.Where(item => item.Contains("00") || item.Contains("mm")).ToList();
                for (int i = 0; i < lista.Count; i++)
                {
                    if ((i + 2) % 3 == 0)
                    {
                        opady.Add(Convert.ToDouble(lista[i].Remove(lista[i].Length - 3)));
                    }
                }
            }




        public void PobierzGodziny()
        {
            List<string> L = new List<string>();

            WebClient client = new WebClient();
            string adres = "http://calendar.zoznam.sk/sunset-pl.php?city=756135";
            string html1 = string.Empty;
            html1 = client.DownloadString(adres);
            int start = html1.IndexOf(@"Dzisiaj: Wschód słońca");
            int koniec = html1.IndexOf("</h1>");
            string s123 = html1.Substring(start, koniec - start);
            string[] tablica;
            List<string> lista = new List<string>();
            tablica = s123.Split(' ');
            foreach (var item in tablica)
            {
                if (item.Contains(':'))
                {
                    lista.Add(item);
                }
            }
            L = lista;
             wschód = Convert.ToDateTime(L[1]);
             zachód = Convert.ToDateTime(L[2]);
        }
    }
}
