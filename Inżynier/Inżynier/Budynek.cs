﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inżynier
{
    [Serializable]
    static class Budynek
    {
        static private List<Pokój> pokoje = new List<Pokój>();
        static public void ZaładujPokoje(List<Pokój> listaPokoi, ITeraz.ITeraz iteraz)
        {
            pokoje = listaPokoi;
            foreach (var pokój in pokoje)
            {
                pokój.Iteraz = iteraz;
                foreach (var urządzenie in pokój.PodajUrządzenia())
                {
                    urządzenie.Iteraz = iteraz;
                }
            }
        }
        static public List<Pokój> PodajPokoje()
        {
            return pokoje;
        }
        static public void UsuńPokój(int pozycja)
        {
            try 
	        {	        
                pokoje.RemoveAt(pozycja);
        	}
	        catch (Exception)
            {
                System.Windows.MessageBox.Show("Nie można usunąć pokoju");
	        }
               
        }
        static public void DodajPokój(Pokój pokój)
        {
            pokoje.Add(pokój);
        }
        static public void ReakcjaBudynek()
        {
            foreach (Pokój item in pokoje)
            {
                item.ReakcjaPokój();
            }
        }
        static public void DodajUrządzenie(int Pokój, Urządzenie urządzenie)
        {
            pokoje[Pokój].DodajUrządzenie(urządzenie);
        }
        static public void UsuńUrządzenie(int pokój, int urządzenie)
        {
            pokoje[pokój].UsuńUrządzenie(urządzenie);
        }
        static public void ZmieńUrządzenie(int nrPokoju, int nrUrządzenia, Urządzenie urządzenie)
        {
            pokoje[nrPokoju].ZmieńUrządzenie(nrUrządzenia, urządzenie);
        }
    }
}
