﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
namespace Inżynier
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Timer t = new Timer();
        ITeraz.ITeraz iteraz;
        IOknoPogody.IOknoPogody iOknoPogody;

        public MainWindow()
        {
            InitializeComponent();      
        }
        public MainWindow(ITeraz.ITeraz iteraz, IOknoPogody.IOknoPogody iOknoPogody)
        {
            this.iteraz = iteraz;
            this.iOknoPogody = iOknoPogody;
            InitializeComponent();
            t.Start();
            t.Interval = 1000;
            t.Tick += new EventHandler(timer1_Tick);
            

            Wyświetl();
            
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            iteraz.Aktualizuj();
            
            Budynek.ReakcjaBudynek();
            temp.Content = "temperatura:" + iteraz.Temperatura;
            opad.Content = "opady:" + iteraz.Opady;
            PoraDnia.Content= "pora dnia: " + (iteraz.CzyDzień ? "dzień" : "noc");
            Wyświetl();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            iOknoPogody.WyświetlOkno();
        }
        void Wyświetl()
        {
            oknoDomu.Children.Clear();
            foreach (var item in Budynek.PodajPokoje())
            {
                dodajDoListy(item);
            }
        }
        void dodajDoListy(Pokój pokój)
        {
            StackPanel st = new StackPanel();
            
            TextBlock tb = new TextBlock();
            tb.Text = pokój.Nazwa +" Temp: " +pokój.Temperatura;
            tb.FontSize = 16;
            st.Children.Insert(0, tb);
            int pozycja = oknoDomu.Children.Count;
            var listaUrządzeń = Budynek.PodajPokoje()[pozycja].PodajUrządzenia();
            foreach (var item in listaUrządzeń)
            {
                TextBlock tb1 = new TextBlock();
                
                tb1.Text = item.Nazwa;
                int pozycjaUrządznia = st.Children.Count;

                Ellipse el = new Ellipse();
                el.Width = 10;
                el.Height = 10;
                el.Fill = item.CzyDziałą ? Brushes.Green : (item.CzyReaguje ? Brushes.Yellow : Brushes.Red);
                
                StackPanel sp = new StackPanel();
                sp.Orientation = System.Windows.Controls.Orientation.Vertical;
                sp.Children.Insert(0, tb1);
                sp.Children.Insert(1, el);
                st.Children.Insert(pozycjaUrządznia, sp);
            
            tb1.MouseLeftButtonDown += delegate(object sender, MouseButtonEventArgs e)
            {
                OknoUrządzeń o1 = new OknoUrządzeń(pozycja,pozycjaUrządznia-1);
                o1.Show();
            };
            }
            oknoDomu.Children.Insert(oknoDomu.Children.Count, st);
            tb.PreviewMouseRightButtonDown += delegate(object sender, MouseButtonEventArgs e)
            {
                if (System.Windows.MessageBox.Show("Czy usunąć pokój?", Budynek.PodajPokoje()[pozycja].Nazwa, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                {
                    
                }
                else
                {
                    Budynek.UsuńPokój(pozycja);
                }
            };
            
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            NowyPokój np = new NowyPokój(this.iteraz);
            np.Show();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (Budynek.PodajPokoje().Count > 0)
            {
                NoweUrządzenie nw = new NoweUrządzenie(this.iteraz);
                nw.Show();
            }
            else System.Windows.MessageBox.Show("Najpierw dodaj pokój");
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            iOknoPogody.WyświetlOkno();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            BinaryFormatter formater = new BinaryFormatter();
            Stream stream = File.Open("plikdomu.sid", FileMode.Create);
            formater.Serialize(stream, Budynek.PodajPokoje());
            stream.Close();
            System.Windows.MessageBox.Show("Zapisano");
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            try
            {

            BinaryFormatter formater = new BinaryFormatter();
            Stream stream = File.Open("plikdomu.sid", FileMode.Open);
            Budynek.ZaładujPokoje ((List<Pokój>) formater.Deserialize(stream), iteraz);
            stream.Close();
            System.Windows.MessageBox.Show("Wczytano");
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("Nie można odczytać pliku");
            }
        }
    }
}
