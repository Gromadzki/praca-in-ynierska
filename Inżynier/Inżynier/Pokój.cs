﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inżynier
{
[Serializable]
    public class Pokój
    {
        private string nazwa;
        public string Nazwa
        {
            get { return nazwa; }
            set { nazwa = value; }
        }
        private List<Urządzenie> urządzenia = new List<Urządzenie>();
        private int temperatura;
        public int Temperatura
        {
            get { return temperatura; }
        }
        private bool czyJasno;
        public bool CzyJasno
        {
            get { return czyJasno; }
        }

        ITeraz.ITeraz iteraz;
        public ITeraz.ITeraz Iteraz { set { iteraz = value; } }
        public Pokój(string Nazwa, ITeraz.ITeraz iteraz)
        {
            nazwa = Nazwa;
            this.iteraz = iteraz;
            temperatura = iteraz.Temperatura;
        }
    

        public List<Urządzenie> PodajUrządzenia() 
        { 
            return urządzenia; 
        }
        public void UsuńUrządzenie(int pozycja) 
        {
            try
            {
                urządzenia.RemoveAt(pozycja);
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("Nie można usunąć urządzenia");
            }
        }
        public void DodajUrządzenie(Urządzenie urządzenie) 
        {
            urządzenia.Add(urządzenie);
        }
        public void ReakcjaPokój()
        {
            if (iteraz.Temperatura < temperatura)
            {
                temperatura -= 1;
            }
            else if (iteraz.Temperatura > temperatura)
            {
                temperatura += 1;
            }
            foreach (Urządzenie item in urządzenia)
            {
                item.Reaguj(temperatura);
                if (item.CzyKlimatyzator && item.CzyDziałą)
                {
                    if (item.temp2+1 < temperatura)
                    {
                        temperatura -= 3;
                    }
                    else if (item.temp1-1 > temperatura)
                    {
                        temperatura += 3;
                    }
                }
            }
        }
        public void ZmieńUrządzenie(int nrUrządzenia, Urządzenie urządzenie)
        {
            try
            {
                urządzenia[nrUrządzenia] = urządzenie;
            }
            catch (Exception)
            {
                System.Windows.MessageBox.Show("Aktualizacja urządzenia nie powiodła się");
            }
        }
        public bool CzyJestKlimatyzator()
        {
            foreach (var item in urządzenia)
            {
                if (item.CzyKlimatyzator)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
