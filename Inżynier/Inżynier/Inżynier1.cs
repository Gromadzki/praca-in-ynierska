﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Inżynier
{
    public class Inżynier1 : IInżynier.IInżynier
    {
        ITeraz.ITeraz iTeraz;
        IOknoPogody.IOknoPogody iOknoPogody;

        public Inżynier1(ITeraz.ITeraz iTeraz, IOknoPogody.IOknoPogody iOknoPogody)
        {
            this.iTeraz = iTeraz;
            this.iOknoPogody = iOknoPogody;
        }
        
        public void uruchom()
        {
            Application ap = new Application();
            ap.Run(new MainWindow(iTeraz, iOknoPogody));
        }
    }
}
