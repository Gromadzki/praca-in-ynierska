﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Inżynier
{
    /// <summary>
    /// Interaction logic for NoweUrządzenie.xaml
    /// </summary>
    public partial class NoweUrządzenie : Window
    {
        public NoweUrządzenie()
        {
            InitializeComponent();
        }
        
        ITeraz.ITeraz iteraz;
        public NoweUrządzenie(ITeraz.ITeraz iteraz)
        {
            InitializeComponent();
            this.iteraz = iteraz;
            foreach (var item in Budynek.PodajPokoje())
            {
                ComboBoxItem cbi = new ComboBoxItem();
                cbi.Content= item.Nazwa;
                CB.Items.Add(cbi);
            }
            CB.SelectedIndex = 0;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Nazwa.Text.Length == 0)
            {
                MessageBox.Show("Podaj nazwę urządzenia");
            }
            else
            {

                Urządzenie urządzenie = new Urządzenie(Nazwa.Text, iteraz);
                if ((bool)Klimatyzator.IsChecked)
                {
                    urządzenie.CzyKlimatyzator = true;
                    if (Budynek.PodajPokoje()[CB.SelectedIndex].CzyJestKlimatyzator())
                    {
                        MessageBox.Show("W pokoju jest już klimatyzator");
                    }
                    else
                    {

                        Budynek.DodajUrządzenie(CB.SelectedIndex, urządzenie);
                        this.Close();
                    }
                }
                else
                {
                    Budynek.DodajUrządzenie(CB.SelectedIndex, urządzenie);
                    this.Close();
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Klimatyzator_Checked(object sender, RoutedEventArgs e)
        {
            Nazwa.Text = "Klimatyzator";
            Nazwa.IsEnabled = false;
        }

        private void Klimatyzator_Unchecked(object sender, RoutedEventArgs e)
        {
            Nazwa.IsEnabled = true;
        }
        
    }
}
