﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Inżynier
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class OknoUrządzeń : Window
    {
        int nrPokoju;
        int nrUrządzenia;
        Urządzenie urządzenie;

        public OknoUrządzeń()
        {
            InitializeComponent();
        }
        public OknoUrządzeń(int nrPokoju, int nrUrządzenia)
        {
            this.nrPokoju = nrPokoju;
            this.nrUrządzenia = nrUrządzenia;
            urządzenie = Budynek.PodajPokoje()[nrPokoju].PodajUrządzenia()[nrUrządzenia];
            InitializeComponent();
            Title = urządzenie.Nazwa;
            Załaduj();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Budynek.ZmieńUrządzenie(nrPokoju, nrUrządzenia, urządzenie);
            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Budynek.UsuńUrządzenie(nrPokoju, nrUrządzenia);
            this.Close();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            urządzenie.WDzień = true;
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            urządzenie.WDzień = false;
        }

        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)
        {
            urządzenie.WczasieDeszczu = true;
        }

        private void RadioButton_Checked_3(object sender, RoutedEventArgs e)
        {
            urządzenie.WczasieDeszczu = false;
        }

        private void RadioButton_Checked_4(object sender, RoutedEventArgs e)
        {
            urządzenie.CzyDziałą = true;
        }

        private void RadioButton_Checked_5(object sender, RoutedEventArgs e)
        {
            urządzenie.CzyDziałą = false;
        }

        private void RadioButton_Checked_6(object sender, RoutedEventArgs e)
        {
            urządzenie.Start = true;
        }

        private void RadioButton_Checked_7(object sender, RoutedEventArgs e)
        {
            urządzenie.Start = false;
        }

        private void RadioButton_Checked_8(object sender, RoutedEventArgs e)
        {
            urządzenie.CzyReaguje = true;
            Włączone.IsEnabled = false;
            Wyłączone.IsEnabled = false;
        }

        private void RadioButton_Checked_9(object sender, RoutedEventArgs e)
        {
            urządzenie.CzyReaguje = false;
            Włączone.IsEnabled = true;
            Wyłączone.IsEnabled = true;
        }

        private void RadioButton_Checked_10(object sender, RoutedEventArgs e)
        {
            urządzenie.ReakcjaNaTemperaturę = true;
            urządzenie.ReakcjaNaŚwiatło = false;
            urządzenie.ReakcjaNaOpady = false;
            urządzenie.ReakcjaNaTemperaturęWPokoju = false;

        }

        private void RadioButton_Checked_11(object sender, RoutedEventArgs e)
        {
            urządzenie.ReakcjaNaTemperaturę = false;
            urządzenie.ReakcjaNaŚwiatło = false;
            urządzenie.ReakcjaNaOpady = true;
            urządzenie.ReakcjaNaTemperaturęWPokoju = false;

        }

        private void RadioButton_Checked_12(object sender, RoutedEventArgs e)
        {

            urządzenie.ReakcjaNaTemperaturę = false;
            urządzenie.ReakcjaNaŚwiatło = true;
            urządzenie.ReakcjaNaOpady = false;
            urządzenie.ReakcjaNaTemperaturęWPokoju = false;

        }
        private void Załaduj()
        {
            if (urządzenie.CzyReaguje)
                Automatyczny.IsChecked = true;
            else
                Manualny.IsChecked = true;

            if (urządzenie.ReakcjaNaTemperaturę)
                Temperatura.IsChecked=true;
            if (urządzenie.ReakcjaNaOpady)
                Opady.IsChecked = true;
            if (urządzenie.ReakcjaNaŚwiatło)
                Światło.IsChecked = true;

            if (urządzenie.Start)
                Włączanie.IsChecked = true;
            else
                Wyłączanie.IsChecked = true;


            if (urządzenie.CzyDziałą)
                Włączone.IsChecked = true;
            else
                Wyłączone.IsChecked = true;

            if (urządzenie.WczasieDeszczu)
                Deszczowo.IsChecked = true;
            else
                BezOpadów.IsChecked = true;

            if (urządzenie.WDzień)
                Jasno.IsChecked = true;
            else
                Ciemno.IsChecked = true;
            Temp1slider.Value = urządzenie.temp1;
            Temp2slider.Value = urządzenie.temp2;

            
        }

        private void TemperaturaWnętrza_Checked(object sender, RoutedEventArgs e)
        {
            urządzenie.ReakcjaNaTemperaturę = false;
            urządzenie.ReakcjaNaŚwiatło = false;
            urządzenie.ReakcjaNaOpady = false;
            urządzenie.ReakcjaNaTemperaturęWPokoju = true;
        }

        private void Temp1slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            urządzenie.temp1 = Convert.ToInt16(Math.Round(Temp1slider.Value, 0));
            tempmin.Content = "Temp. minimalna: " + urządzenie.temp1.ToString();
        }

        private void Temp2slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            urządzenie.temp2 = Convert.ToInt16(Math.Round(Temp2slider.Value, 0));
            tempmax.Content = "Temp. maksymalna: " + urządzenie.temp2.ToString();
        }
       
    }
}
