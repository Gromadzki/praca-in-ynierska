﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inżynier
{
    [Serializable]
    public class Urządzenie
    {
        private string nazwa;
        public string Nazwa
        {
            get { return nazwa; }
            set { nazwa = value; }
        }
        public bool CzyDziałą = false;           // czy aktywne
        public bool CzyReaguje = false;   //czy auto/manual
        public bool CzyKlimatyzator = false;
        public bool ReakcjaNaTemperaturę = true;
        public bool ReakcjaNaOpady = false;
        public bool ReakcjaNaŚwiatło = false;
        public bool ReakcjaNaTemperaturęWPokoju = false;
        public int temp1 = 10;
        public int temp2 = 10;
        public bool WczasieDeszczu = false; //true - w czasie deszczu, false - gdy sucho
        public bool WDzień = false; //true - dzień, false - noc
        public bool Start = false; //true - uruchamia, false - wyłącza
        
        ITeraz.ITeraz iteraz;
        public ITeraz.ITeraz Iteraz { set { iteraz = value; } }



        public Urządzenie(string Nazwa, ITeraz.ITeraz iteraz)
        {
            nazwa = Nazwa;
            this.iteraz = iteraz;
        }


        public void Reaguj(int temperaturaPokoju)
        {
            if (CzyReaguje)
            {
                if (ReakcjaNaTemperaturę)
                {
                    if (iteraz.Temperatura >= temp1 && iteraz.Temperatura <= temp2)
                    {
                        if (Start)
                        {
                            CzyDziałą = true;
                        }
                        else
                        {
                            CzyDziałą = false;
                        }
                    }
                    else
                    {
                        if (Start)
                        {
                            CzyDziałą = false;
                        }
                        else
                        {
                            CzyDziałą = true;
                        }
                    }
                }
                else if (ReakcjaNaOpady)
                {
                    if (WczasieDeszczu && iteraz.Opady > 0)
                    {
                        if (Start)
                        {
                            CzyDziałą = true;
                        }
                        else
                        {
                            CzyDziałą = false;
                        }
                    }
                    else if (!WczasieDeszczu && iteraz.Opady == 0)
                    {
                        if (Start)
                        {
                            CzyDziałą = true;
                        }
                        else
                        {
                            CzyDziałą = false;
                        }

                    }
                    else
                    {
                        if (Start)
                        {
                            CzyDziałą = false;
                        }
                        else
                        {
                            CzyDziałą = true;
                        }
                    }
                }
                else if (ReakcjaNaŚwiatło)
                {
                    if (WDzień && iteraz.CzyDzień)
                    {
                        if (Start)
                        {
                            CzyDziałą = true;
                        }
                        else
                        {
                            CzyDziałą = false;
                        }
                    }
                    else if (!WDzień && !iteraz.CzyDzień)
                    {
                        if (Start)
                        {
                            CzyDziałą = true;
                        }
                        else
                        {
                            CzyDziałą = false;
                        }
                    }
                    else
                    {
                        if (Start)
                        {
                            CzyDziałą = false;
                        }
                        else
                        {
                            CzyDziałą = true;
                        }
                    }
                }
                else if (ReakcjaNaTemperaturęWPokoju)
                {
                    if (temperaturaPokoju >= temp1 && temperaturaPokoju <= temp2)
                    {
                        if (Start)
                        {
                            CzyDziałą = true;
                        }
                        else
                        {
                            CzyDziałą = false;
                        }
                    }
                    else
                    {
                        if (Start)
                        {
                            CzyDziałą = false;
                        }
                        else
                        {
                            CzyDziałą = true;
                        }
                    }


                }
            }
        }

           
    }
}
