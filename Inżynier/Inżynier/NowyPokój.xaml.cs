﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Inżynier
{
    /// <summary>
    /// Interaction logic for NowyPokój.xaml
    /// </summary>
    public partial class NowyPokój : Window
    {
        public NowyPokój()
        {
            InitializeComponent();
        }
        ITeraz.ITeraz iteraz;
        public NowyPokój(ITeraz.ITeraz iteraz)
        {
            InitializeComponent();
            this.iteraz = iteraz;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Nazwa.Text.Length == 0)
            {
                MessageBox.Show("Podaj nazwę pokoju");
            }
            else
            {
                Budynek.DodajPokój(new Pokój(Nazwa.Text, iteraz));
                this.Close();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
           
        }
       
    }
}
