﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITeraz
{

    public enum Tryby
    {
        Internet,
        Własny,
        Losowy

    };
    public interface ITeraz
    {
        Tryby Tryb { get; set; }
        int Temperatura { get; set; }
        double Opady { get; set; }
        bool CzyDzień { get; set; }
        void Aktualizuj();
    }
    
}
