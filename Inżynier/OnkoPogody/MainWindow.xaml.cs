﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OnkoPogody
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int temperatura;
        double opady;
        IPogodynka.IPogodynka pogodynka;
        ITeraz.ITeraz teraz;
        public MainWindow()
        {
            InitializeComponent();
        }

        public MainWindow(IPogodynka.IPogodynka pogodynka, ITeraz.ITeraz teraz)
        {
            
            InitializeComponent();
            this.pogodynka = pogodynka;
            this.teraz = teraz;
            temperatura = teraz.Temperatura;
            tempwynik.Text = temperatura.ToString();
            temp.Value = temperatura;
            opady = teraz.Opady;
            opadywynik.Text = opady.ToString();
            Własne.IsChecked = true;
            
        }

        private void temp_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            temperatura = Convert.ToInt16(Math.Round(temp.Value, 0));
            teraz.Temperatura = temperatura;
            tempwynik.Text = temperatura.ToString();
        }

        private void opady_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            opady = Convert.ToDouble(Math.Round(opad.Value, 1));
            teraz.Opady = opady;
            opadywynik.Text = opady.ToString();
        }

        private void Własne_Checked(object sender, RoutedEventArgs e)
        {
            temp.IsEnabled = true;
            opad.IsEnabled = true;
            Dzień.IsEnabled = true;
            Noc.IsEnabled = true;
            teraz.Tryb = ITeraz.Tryby.Własny;
        }

        private void Własne_Unchecked(object sender, RoutedEventArgs e)
        {
            temp.IsEnabled = false;
            opad.IsEnabled = false;
            Dzień.IsEnabled = false;
            Noc.IsEnabled = false;
        }

        private void Losowe_Checked(object sender, RoutedEventArgs e)
        {
            teraz.Tryb = ITeraz.Tryby.Losowy;
           // tempwynik.Text = pogodynka.ZwróćTemperaturę(0).ToString();
        }

        private void Internet_Checked(object sender, RoutedEventArgs e)
        {
            teraz.Tryb = ITeraz.Tryby.Internet;
            Dzień.IsChecked = false;
            Noc.IsChecked = false;

        }

        private void Dzień_Checked(object sender, RoutedEventArgs e)
        {
            teraz.CzyDzień = true;
        }

        private void Noc_Checked(object sender, RoutedEventArgs e)
        {
            teraz.CzyDzień = false;
        }

    }
}
