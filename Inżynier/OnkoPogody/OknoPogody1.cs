﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace OnkoPogody
{
    public class OknoPogody1 : IOknoPogody.IOknoPogody
    {
        IPogodynka.IPogodynka iPogodynka;
        ITeraz.ITeraz iTeraz;
        public  OknoPogody1(IPogodynka.IPogodynka iPogodynka, ITeraz.ITeraz iTeraz)
        {
            this.iPogodynka = iPogodynka;
            this.iTeraz = iTeraz;
        }
        public void WyświetlOkno()
        {
            MainWindow mw = new MainWindow(iPogodynka, iTeraz);
            mw.Show();
            

        }
    }
}
