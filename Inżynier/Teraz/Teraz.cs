﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Teraz
{
[Serializable]
    public class Teraz : ITeraz.ITeraz
    {
        private int temperatura;
        private double opady;
        private bool czyDzień;
        private ITeraz.Tryby tryb;
        private IPogodynka.IPogodynka iPogodynka;
        private int timer = 60;

        public Teraz(IPogodynka.IPogodynka iPogodynka)
        {
            temperatura = 10;
            opady = 0;
            czyDzień = true;
            tryb = ITeraz.Tryby.Własny;
           this.iPogodynka = iPogodynka; 
        }

        ITeraz.Tryby ITeraz.ITeraz.Tryb
        {
            get
            {
                return tryb;
            }
            set
            {
                tryb = value;
            }
        }

        public int Temperatura
        {
            get
            {
                return temperatura;
            }
            set
            {
                if (value > 50)
                
                    temperatura = 50;
                
                else if (value < -50)
                    temperatura = -50;
                else
                temperatura = value;
            }
        }

        public double Opady
        {
            get
            {
                return opady;
            }
            set
            {
                if (value < 0)
                    opady = 0;
                else
                opady = value;
            }
        }

        public bool CzyDzień
        {
            get
            {
                return czyDzień;
            }
            set
            {
                czyDzień = value;
            }
        }


        public void Aktualizuj() 
        {
            switch (tryb)
            {
                case ITeraz.Tryby.Internet:
                    if (timer != 60) 
                    { timer++; break; }
                    int godzina = DateTime.Now.Hour;
                    //temperatura = iPogodynka.PodajTemperaturę(godzina);
                    opady= iPogodynka.PodajOpady(godzina);
                    czyDzień = iPogodynka.CzyDzień(godzina);
                    timer = 0;
                    break;  
                case ITeraz.Tryby.Własny:
                    timer = 60;
                    break;
                case ITeraz.Tryby.Losowy:
                    timer = 60;
                    losuj();
                    break;
                default:
                    break;
            }
        }
        private void losuj()
        {
            Random r = new Random();
            int p1 = r.Next(1, 101);
            if (p1 < 35)
            {
                temperatura -=1;
            }
            else if (p1 < 70)
            {
                temperatura +=1;
            }
            else if (p1 < 80)
            {
                temperatura -=2;
            }
            else if (p1 < 90)
            {
                temperatura +=2;
            }

            int p2 = r.Next(1, 101);

            if (opady == 0)
            {
                if (p2 < 50)
                {
                    opady +=0.1;
                }
            }
            else
            {
                if (p2 < 5)
                {
                    opady =0;
                }
                else if (p2 < 50)
                {
                    opady +=0.1;
                }
                else if (p2 < 80)
                {
                    opady -=0.1;
                }

            }
            int p3 = r.Next(1, 101);
            if (p3<5)
            {
                if (czyDzień)
                    czyDzień=false;
                        else
                    czyDzień=true;
            }
        }

    }
}