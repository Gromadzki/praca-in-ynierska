﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;
using Castle.MicroKernel.Registration;

namespace Starter
{
    class Starter
    {
        IWindsorContainer kontener = new WindsorContainer();

        public void Start()
        {
            kontener.Register(Component.For<startAplikacji>());
            kontener.Register(Component.For<IPogodynka.IPogodynka>().ImplementedBy<Pogodynka2.Pogodynka2>());
            kontener.Register(Component.For<IOknoPogody.IOknoPogody>().ImplementedBy<OnkoPogody.OknoPogody1>());
            kontener.Register(Component.For<ITeraz.ITeraz>().ImplementedBy<Teraz.Teraz>());
            kontener.Register(Component.For<IInżynier.IInżynier>().ImplementedBy<Inżynier.Inżynier1>());
            var  start = kontener.Resolve<startAplikacji>();

        }
        class startAplikacji {
            IInżynier.IInżynier I2;
            public startAplikacji(IInżynier.IInżynier I22)
            {
                I2 = I22;
                I2.uruchom();
            }
        }
    }
}
